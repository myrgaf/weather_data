import requests
from ip_data import get_location_by_ip


def get_weather_data() -> dict:
    try:
        """ Получение данных о погоде с помощью API"""
        API_ACCESS_KEY = '308255c917f2650f47a82b13f0a0f7db' # Ключ для использования сервиса погоды

        ip_address_data = get_location_by_ip()
        city = ip_address_data.get('city')

        url = f"http://api.weatherstack.com/current?access_key={API_ACCESS_KEY}&query={city}"
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            print(f"Произошла ошибка при запросе погоды")
            return {}
    except (requests.RequestException, KeyError) as e:
        print(f"Произошла ошибка запроса: {e}")
        return {}
