from dotenv import load_dotenv
import requests
import os


load_dotenv()
IPDATA_KEY = os.getenv("IPDATA_KEY") # Ключ для сайта api.apidata


def get_location_by_ip() -> dict:
    """Информация о местоположение и ip"""
    services = ['https://ipinfo.io', 'http://ip-api.com/json', f'https://api.ipdata.co?api-key={IPDATA_KEY}']
    for service in services:
        try:
            response = requests.get(service)
            if response.status_code == 200:
                data = response.json()
                return data
        except requests.RequestException as e:
            print(f"Ошибка при запросе к сервису {service}: {e}")
    raise Exception("Все сервисы недоступны")
