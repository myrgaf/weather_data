from weather_data import get_weather_data
from datetime import datetime
from tabulate import tabulate


def create_table():
    weather_data = get_weather_data()

    city = weather_data['location']['name']
    date = weather_data['location']['localtime']
    """Форматирование времени в нужный формат"""
    time_obj = datetime.strptime(date, '%Y-%m-%d %H:%M')
    real_time = time_obj.strftime('%H:%M')

    temperature = weather_data['current']['temperature']
    wind_speed = weather_data['current']['wind_speed']
    humidity = weather_data['current']['humidity']
    pressure = weather_data['current']['pressure']

    """Организация данных в виде списка списков, требование библиотеки"""
    table = [
        ['Город', city],
        ['Время', real_time],
        ['Температура (C)', temperature],
        ['Скорость ветра (км/ч)', wind_speed],
        ['Влажность (%)', humidity],
        ['Давление (мбар)', pressure]
    ]

    """Вывод данных в виде таблицы"""
    print(tabulate(table, headers=["Параметр", "Значение"], tablefmt="grid"))

    """Расчет точки росы"""
    dew_point = temperature - ((100 - humidity) / 5)

    """Расчет ощущаемой температуры при ветре"""
    wind_chill = (13.12 + 0.6215 * temperature - 11.37 * (wind_speed ** 0.16) +
                  0.3965 * temperature * (wind_speed ** 0.16))

    """Расчета теплового индекса или индекс ощущаемой температуры"""
    heat_index = temperature + ((temperature - 61) * (0.12 + 0.094 * humidity) + 0.2 * wind_speed - 0.5)

    results = [
        ['Точка росы', dew_point],
        ['Ощущаемая температура при ветре', wind_chill],
        ['Тепловой индекс', heat_index]
    ]

    print(tabulate(results, headers=["Параметр", "Значение"], tablefmt="grid"))
