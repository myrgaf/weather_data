from ip_data import get_location_by_ip
from weather_data import get_weather_data
from data_to_table import create_table

if __name__ == '__main__':
    get_location_by_ip()
    get_weather_data()
    create_table()
